package com.galvanize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Algorithm {

    public boolean allEqual(String str) {
        boolean isEqual = true;
        str = str.toLowerCase();

        if (str.length() < 1) {
            isEqual = false;
        }

        for (int i = 1; i < str.length(); i++) {
            if (str.charAt(i) != str.charAt(0)) {
                isEqual = false;
                break;
            }
        }

        return isEqual;
    }

    public Map<String, Long> letterCount(String str) {
        Map<String, Long> resultMap = new HashMap<>();
        str = str.toLowerCase();
        long count = 0;

        for (int i = 0; i < str.length(); i++) {
            if (!resultMap.containsKey(String.valueOf(str.charAt(i)))) {
                for (int j = 0; j < str.length(); j++) {
                    if (str.charAt(i) == str.charAt(j)) {
                        count++;
                    }
                }
                resultMap.put(String.valueOf(str.charAt(i)), count);
                count = 0;
            }

        }

        return resultMap;
    }

    public String interleave(List<String> list, List<String> list2) {
        StringBuilder str = new StringBuilder();
        int listSize = Math.max(list.size(), list2.size());

        if (list.isEmpty() && list2.isEmpty()) {
            return str.toString();
        }

        for (int i = 0; i < listSize; i++) {
            str.append(list.get(i));
            str.append(list2.get(i));
        }

        return str.toString();
    }

}
