package com.galvanize;


import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class AlgorithmTest {

    @Test
    void allEqualShouldReturnTrueIfAllElsInStringAreTheSameAndFalseIfElsAreNotUnique() {
        // Setup
        Algorithm alg = new Algorithm();

        // Enact method
        // alg.allEqual();

        // Assert
        assertTrue(alg.allEqual("aAa"));
        assertFalse(alg.allEqual("bbBbabbbb"));
        assertFalse(alg.allEqual(""));
    }

    @Test
    void letterCountShouldReturnHashMapOfEachUniqueCharAndHowManyTimesTheyAppear() {
        // Setup
        Algorithm alg = new Algorithm();

        // Enact method
        Map<String, Long> expected1 = new HashMap<>();
        Map<String, Long> expected2 = new HashMap<>();
        Map<String, Long> expected3 = new HashMap<>();
        String str1 = "aa";
        expected1.put("a", 2L);
        String str2 = "abBcd";
        expected2.put("d", 1L);
        expected2.put("c", 1L);
        expected2.put("b", 2L);
        expected2.put("a", 1L);
        String str3 = "";
        Map<String, Long> result1 = alg.letterCount(str1);
        Map<String, Long> result2 = alg.letterCount(str2);
        Map<String, Long> result3 = alg.letterCount(str3);

        // Assert
        assertEquals(expected1, result1);
        assertEquals(expected2, result2);
        assertEquals(expected3, result3);
    }

    @Test
    void interleaveShouldMergeTheTwoListsPassedOneElementFromEachListAtATime() {
        // Setup
        Algorithm alg = new Algorithm();

        // Enact
        // => returns "adbecf"
        String result1 = alg.interleave(Arrays.asList("a", "b", "c"), Arrays.asList("d", "e", "f"));
        String expected1 = "adbecf";
        // => returns "abcdef"
        String result2 = alg.interleave(Arrays.asList("a", "c", "e"), Arrays.asList("b", "d", "f"));
        String expected2 = "abcdef";
        // => returns ""
        String result3 = alg.interleave(Collections.emptyList(), Collections.emptyList());
        String expected3 = "";

        // Assert
        assertEquals(expected1, result1);
        assertEquals(expected2, result2);
        assertEquals(expected3, result3);


    }

}
